CREATE TABLE opty_sellout.sm_inventario (
	TpMov				 	DECIMAL(2,0),
	Catena				VARCHAR(100),
    CodicePDV				VARCHAR(10),
	Data_Inventario		VARCHAR(8),
	EAN					VARCHAR(13),
	Qta					DECIMAL(6,0)
);
ALTER TABLE opty_sellout.sm_inventario ADD CONSTRAINT PK_sm_inventario PRIMARY KEY(TpMov,CodicePDV,EAN) ENABLE;

CREATE TABLE opty_sellout.sm_rating_titolo (
	EAN					VARCHAR(13),
	Rating				DECIMAL(6,3),
	Classe				VARCHAR(1)
);
ALTER TABLE opty_sellout.sm_rating_titolo ADD CONSTRAINT PK_sm_rating PRIMARY KEY(EAN) ENABLE;

CREATE TABLE opty_sellout.sm_cluster_pdv (
	Catena				VARCHAR(100),
	CodicePDV				VARCHAR(10),
	Cluster				VARCHAR(1),
	Riordino				DECIMAL(3,0),
	Sottoscorta			DECIMAL(3,0)
);
ALTER TABLE opty_sellout.sm_cluster_pdv ADD CONSTRAINT PK_sm_cluster_pdv PRIMARY KEY(CodicePDV) ENABLE;

CREATE TABLE opty_sellout.sm_an_cluster_pdv (
	Catena			    VARCHAR(100),
	Cluster				VARCHAR(1),
	Capienza				DECIMAL(3,0)
);
ALTER TABLE opty_sellout.sm_an_cluster_pdv ADD CONSTRAINT PK_sm_an_cluster_pdv PRIMARY KEY(Catena,Cluster) ENABLE;

CREATE TABLE opty_sellout.sm_eccezioni (
	Catena			    VARCHAR(100),
	EAN					VARCHAR(13),
	Anno				    DECIMAL(4,0),
	Settimana             DECIMAL(2,0)
);
ALTER TABLE opty_sellout.sm_eccezioni ADD CONSTRAINT PK_sm_eccezioni PRIMARY KEY(Catena,EAN,Anno,Settimana) ENABLE;

CREATE TABLE opty_sellout.sm_incidenze (
	Catena			    VARCHAR(100),
	Cluster				VARCHAR(13),
	Genere			    VARCHAR(100),
	Allocazione			DECIMAL(4,0)
);
ALTER TABLE opty_sellout.sm_incidenze ADD CONSTRAINT PK_sm_incidenze PRIMARY KEY(Catena,Cluster,Genere) ENABLE;

CREATE TABLE opty_sellout.sm_rese (
	TpMov				 	DECIMAL(2,0),
	Catena			    VARCHAR(100),
	CodicePDV				VARCHAR(10),
	EAN					VARCHAR(13),
	Qta					DECIMAL(6,0),
	Data_resa				VARCHAR(8),
	Anno_resa				DECIMAL(4,0),
	Settimana_resa        DECIMAL(2,0)
);
ALTER TABLE opty_sellout.sm_rese ADD CONSTRAINT PK_sm_rese PRIMARY KEY(CodicePDV,EAN,Data_resa) ENABLE;

CREATE or replace TABLE opty_sellout.sm_sellout (
	TpMov				 	DECIMAL(2,0),
	Catena			    VARCHAR(100),
	CodicePDV				VARCHAR(10),
	EAN					VARCHAR(13),
	Qta					DECIMAL(6,0),
	Data_sellout				VARCHAR(8),
	Anno_sellout				DECIMAL(4,0),
	Settimana_sellout        DECIMAL(2,0)
);
ALTER TABLE opty_sellout.sm_sellout ADD CONSTRAINT PK_sm_sellout PRIMARY KEY(CodicePDV,EAN,Data_sellout) ENABLE;

CREATE or replace TABLE opty_sellout.sm_sellin (
	TpMov				 	DECIMAL(2,0),
	Catena			    VARCHAR(100),
	CodicePDV				VARCHAR(10),
	EAN					VARCHAR(13),
	Qta					DECIMAL(6,0),
	Data_sellin				VARCHAR(8),
	Anno_sellin				DECIMAL(4,0),
	Settimana_sellin        DECIMAL(2,0)
);
ALTER TABLE opty_sellout.sm_sellin ADD CONSTRAINT PK_sm_sellin PRIMARY KEY(CodicePDV,EAN,Data_sellin) ENABLE;

CREATE or replace TABLE opty_sellout.sm_utenti (
	Utente			    VARCHAR(100),
	Password				VARCHAR(100)
);
ALTER TABLE opty_sellout.sm_utenti ADD CONSTRAINT PK_sm_utenti PRIMARY KEY(Utente) ENABLE;
commit;