package com.thinkopen.emmelibri.model;

import javax.persistence.*;
import java.math.BigInteger;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_sellin", schema = "opty_sellout", catalog = "EXA_DB")
@IdClass(SmSellinEntityPK.class)
public class SmSellinEntity {
    private BigInteger tpmov;
    private String catena;
    private String codicepdv;
    private String ean;
    private int qta;
    private String dataSellin;
    private String annoSellin;
    private String settimanaSellin;

    @Basic
    @Column(name = "tpmov", nullable = true, insertable = true, updatable = true, precision = 0)
    public BigInteger getTpmov() {
        return tpmov;
    }

    public void setTpmov(BigInteger tpmov) {
        this.tpmov = tpmov;
    }

    @Basic
    @Column(name = "catena", nullable = true, insertable = true, updatable = true, length = 100)
    public String getCatena() {
        return catena;
    }

    public void setCatena(String catena) {
        this.catena = catena;
    }

    @Id
    @Column(name = "codicepdv", nullable = false, insertable = true, updatable = true, length = 10)
    public String getCodicepdv() {
        return codicepdv;
    }

    public void setCodicepdv(String codicepdv) {
        this.codicepdv = codicepdv;
    }

    @Id
    @Column(name = "ean", nullable = false, insertable = true, updatable = true, length = 13)
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Id
    @Column(name = "qta", nullable = false, insertable = true, updatable = true, precision = 0)
    public int getQta() {
        return qta;
    }

    public void setQta(int qta) {
        this.qta = qta;
    }

    @Id
    @Column(name = "data_sellin", nullable = false, insertable = true, updatable = true, length = 8)
    public String getDataSellin() {
        return dataSellin;
    }

    public void setDataSellin(String dataSellin) {
        this.dataSellin = dataSellin;
    }

    @Basic
    @Column(name = "anno_sellin", nullable = true, insertable = true, updatable = true, length = 4)
    public String getAnnoSellin() {
        return annoSellin;
    }

    public void setAnnoSellin(String annoSellin) {
        this.annoSellin = annoSellin;
    }

    @Basic
    @Column(name = "settimana_sellin", nullable = true, insertable = true, updatable = true, length = 2)
    public String getSettimanaSellin() {
        return settimanaSellin;
    }

    public void setSettimanaSellin(String settimanaSellin) {
        this.settimanaSellin = settimanaSellin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmSellinEntity that = (SmSellinEntity) o;

        if (qta != that.qta) return false;
        if (annoSellin != null ? !annoSellin.equals(that.annoSellin) : that.annoSellin != null) return false;
        if (catena != null ? !catena.equals(that.catena) : that.catena != null) return false;
        if (codicepdv != null ? !codicepdv.equals(that.codicepdv) : that.codicepdv != null) return false;
        if (dataSellin != null ? !dataSellin.equals(that.dataSellin) : that.dataSellin != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (settimanaSellin != null ? !settimanaSellin.equals(that.settimanaSellin) : that.settimanaSellin != null)
            return false;
        if (tpmov != null ? !tpmov.equals(that.tpmov) : that.tpmov != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tpmov != null ? tpmov.hashCode() : 0;
        result = 31 * result + (catena != null ? catena.hashCode() : 0);
        result = 31 * result + (codicepdv != null ? codicepdv.hashCode() : 0);
        result = 31 * result + (ean != null ? ean.hashCode() : 0);
        result = 31 * result + qta;
        result = 31 * result + (dataSellin != null ? dataSellin.hashCode() : 0);
        result = 31 * result + (annoSellin != null ? annoSellin.hashCode() : 0);
        result = 31 * result + (settimanaSellin != null ? settimanaSellin.hashCode() : 0);
        return result;
    }
}
