package com.thinkopen.emmelibri.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by donato on 9/19/17.
 */
public class SmEccezioniEntityPK implements Serializable {
    private String catena;

    @Column(name = "catena", nullable = false, insertable = true, updatable = true, length = 100)
    @Id
    public String getCatena() {
        return catena;
    }

    public void setCatena(String catena) {
        this.catena = catena;
    }

    private String ean;

    @Column(name = "ean", nullable = false, insertable = true, updatable = true, length = 13)
    @Id
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    private int anno;

    @Column(name = "anno", nullable = false, insertable = true, updatable = true, precision = 0)
    @Id
    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    private BigInteger settimana;

    @Column(name = "settimana", nullable = false, insertable = true, updatable = true, precision = 0)
    @Id
    public BigInteger getSettimana() {
        return settimana;
    }

    public void setSettimana(BigInteger settimana) {
        this.settimana = settimana;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmEccezioniEntityPK that = (SmEccezioniEntityPK) o;

        if (anno != that.anno) return false;
        if (catena != null ? !catena.equals(that.catena) : that.catena != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (settimana != null ? !settimana.equals(that.settimana) : that.settimana != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = catena != null ? catena.hashCode() : 0;
        result = 31 * result + (ean != null ? ean.hashCode() : 0);
        result = 31 * result + anno;
        result = 31 * result + (settimana != null ? settimana.hashCode() : 0);
        return result;
    }
}
