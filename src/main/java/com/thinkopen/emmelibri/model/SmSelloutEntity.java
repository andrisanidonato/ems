package com.thinkopen.emmelibri.model;

import javax.persistence.*;
import java.math.BigInteger;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_sellout", schema = "opty_sellout", catalog = "EXA_DB")
@IdClass(SmSelloutEntityPK.class)
public class SmSelloutEntity {
    private BigInteger tpmov;
    private String catena;
    private String codicepdv;
    private String ean;
    private int qta;
    private String dataSellout;
    private String annoSellout;
    private String settimanaSellout;

    @Basic
    @Column(name = "tpmov", nullable = true, insertable = true, updatable = true, precision = 0)
    public BigInteger getTpmov() {
        return tpmov;
    }

    public void setTpmov(BigInteger tpmov) {
        this.tpmov = tpmov;
    }

    @Basic
    @Column(name = "catena", nullable = true, insertable = true, updatable = true, length = 100)
    public String getCatena() {
        return catena;
    }

    public void setCatena(String catena) {
        this.catena = catena;
    }

    @Id
    @Column(name = "codicepdv", nullable = false, insertable = true, updatable = true, length = 10)
    public String getCodicepdv() {
        return codicepdv;
    }

    public void setCodicepdv(String codicepdv) {
        this.codicepdv = codicepdv;
    }

    @Id
    @Column(name = "ean", nullable = false, insertable = true, updatable = true, length = 13)
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Id
    @Column(name = "qta", nullable = false, insertable = true, updatable = true, precision = 0)
    public int getQta() {
        return qta;
    }

    public void setQta(int qta) {
        this.qta = qta;
    }

    @Id
    @Column(name = "data_sellout", nullable = false, insertable = true, updatable = true, length = 8)
    public String getDataSellout() {
        return dataSellout;
    }

    public void setDataSellout(String dataSellout) {
        this.dataSellout = dataSellout;
    }

    @Basic
    @Column(name = "anno_sellout", nullable = true, insertable = true, updatable = true, length = 4)
    public String getAnnoSellout() {
        return annoSellout;
    }

    public void setAnnoSellout(String annoSellout) {
        this.annoSellout = annoSellout;
    }

    @Basic
    @Column(name = "settimana_sellout", nullable = true, insertable = true, updatable = true, length = 2)
    public String getSettimanaSellout() {
        return settimanaSellout;
    }

    public void setSettimanaSellout(String settimanaSellout) {
        this.settimanaSellout = settimanaSellout;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmSelloutEntity that = (SmSelloutEntity) o;

        if (qta != that.qta) return false;
        if (annoSellout != null ? !annoSellout.equals(that.annoSellout) : that.annoSellout != null) return false;
        if (catena != null ? !catena.equals(that.catena) : that.catena != null) return false;
        if (codicepdv != null ? !codicepdv.equals(that.codicepdv) : that.codicepdv != null) return false;
        if (dataSellout != null ? !dataSellout.equals(that.dataSellout) : that.dataSellout != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (settimanaSellout != null ? !settimanaSellout.equals(that.settimanaSellout) : that.settimanaSellout != null)
            return false;
        if (tpmov != null ? !tpmov.equals(that.tpmov) : that.tpmov != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tpmov != null ? tpmov.hashCode() : 0;
        result = 31 * result + (catena != null ? catena.hashCode() : 0);
        result = 31 * result + (codicepdv != null ? codicepdv.hashCode() : 0);
        result = 31 * result + (ean != null ? ean.hashCode() : 0);
        result = 31 * result + qta;
        result = 31 * result + (dataSellout != null ? dataSellout.hashCode() : 0);
        result = 31 * result + (annoSellout != null ? annoSellout.hashCode() : 0);
        result = 31 * result + (settimanaSellout != null ? settimanaSellout.hashCode() : 0);
        return result;
    }
}
