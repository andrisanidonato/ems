package com.thinkopen.emmelibri.model;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_rating_titolo", schema = "opty_sellout", catalog = "EXA_DB")
public class SmRatingTitoloEntity {
    private String ean;
    private BigDecimal rating;
    private String classe;

    @Id
    @Column(name = "ean", nullable = false, insertable = true, updatable = true, length = 13)
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Basic
    @Column(name = "rating", nullable = true, insertable = true, updatable = true, precision = 3)
    public BigDecimal getRating() {
        return rating;
    }

    public void setRating(BigDecimal rating) {
        this.rating = rating;
    }

    @Basic
    @Column(name = "classe", nullable = true, insertable = true, updatable = true, length = 1)
    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmRatingTitoloEntity that = (SmRatingTitoloEntity) o;

        if (classe != null ? !classe.equals(that.classe) : that.classe != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (rating != null ? !rating.equals(that.rating) : that.rating != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ean != null ? ean.hashCode() : 0;
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + (classe != null ? classe.hashCode() : 0);
        return result;
    }
}
