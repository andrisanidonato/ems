package com.thinkopen.emmelibri.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by donato on 9/19/17.
 */
public class SmSelloutEntityPK implements Serializable {
    private String codicepdv;

    @Column(name = "codicepdv", nullable = false, insertable = true, updatable = true, length = 10)
    @Id
    public String getCodicepdv() {
        return codicepdv;
    }

    public void setCodicepdv(String codicepdv) {
        this.codicepdv = codicepdv;
    }

    private String ean;

    @Column(name = "ean", nullable = false, insertable = true, updatable = true, length = 13)
    @Id
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    private int qta;

    @Column(name = "qta", nullable = false, insertable = true, updatable = true, precision = 0)
    @Id
    public int getQta() {
        return qta;
    }

    public void setQta(int qta) {
        this.qta = qta;
    }

    private String dataSellout;

    @Column(name = "data_sellout", nullable = false, insertable = true, updatable = true, length = 8)
    @Id
    public String getDataSellout() {
        return dataSellout;
    }

    public void setDataSellout(String dataSellout) {
        this.dataSellout = dataSellout;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmSelloutEntityPK that = (SmSelloutEntityPK) o;

        if (qta != that.qta) return false;
        if (codicepdv != null ? !codicepdv.equals(that.codicepdv) : that.codicepdv != null) return false;
        if (dataSellout != null ? !dataSellout.equals(that.dataSellout) : that.dataSellout != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codicepdv != null ? codicepdv.hashCode() : 0;
        result = 31 * result + (ean != null ? ean.hashCode() : 0);
        result = 31 * result + qta;
        result = 31 * result + (dataSellout != null ? dataSellout.hashCode() : 0);
        return result;
    }
}
