package com.thinkopen.emmelibri.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by donato on 9/19/17.
 */
public class SmIncidenzeEntityPK implements Serializable {
    private String catena;
    private String cluster;
    private String genere;

    @Column(name = "catena", nullable = false, insertable = true, updatable = true, length = 100)
    @Id
    public String getCatena() {
        return catena;
    }

    public void setCatena(String catena) {
        this.catena = catena;
    }

    @Column(name = "cluster", nullable = false, insertable = true, updatable = true, length = 13)
    @Id
    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    @Column(name = "genere", nullable = false, insertable = true, updatable = true, length = 100)
    @Id
    public String getGenere() {
        return genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmIncidenzeEntityPK that = (SmIncidenzeEntityPK) o;

        if (catena != null ? !catena.equals(that.catena) : that.catena != null) return false;
        if (cluster != null ? !cluster.equals(that.cluster) : that.cluster != null) return false;
        if (genere != null ? !genere.equals(that.genere) : that.genere != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = catena != null ? catena.hashCode() : 0;
        result = 31 * result + (cluster != null ? cluster.hashCode() : 0);
        result = 31 * result + (genere != null ? genere.hashCode() : 0);
        return result;
    }
}
