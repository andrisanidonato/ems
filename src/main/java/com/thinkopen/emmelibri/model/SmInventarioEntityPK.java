package com.thinkopen.emmelibri.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by donato on 9/19/17.
 */
public class SmInventarioEntityPK implements Serializable {
    private BigInteger tpmov;
    private String codicepdv;
    private String ean;

    @Column(name = "tpmov", nullable = false, insertable = true, updatable = true, precision = 0)
    @Id
    public BigInteger getTpmov() {
        return tpmov;
    }

    public void setTpmov(BigInteger tpmov) {
        this.tpmov = tpmov;
    }

    @Column(name = "codicepdv", nullable = false, insertable = true, updatable = true, length = 10)
    @Id
    public String getCodicepdv() {
        return codicepdv;
    }

    public void setCodicepdv(String codicepdv) {
        this.codicepdv = codicepdv;
    }

    @Column(name = "ean", nullable = false, insertable = true, updatable = true, length = 13)
    @Id
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmInventarioEntityPK that = (SmInventarioEntityPK) o;

        if (codicepdv != null ? !codicepdv.equals(that.codicepdv) : that.codicepdv != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (tpmov != null ? !tpmov.equals(that.tpmov) : that.tpmov != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tpmov != null ? tpmov.hashCode() : 0;
        result = 31 * result + (codicepdv != null ? codicepdv.hashCode() : 0);
        result = 31 * result + (ean != null ? ean.hashCode() : 0);
        return result;
    }
}
