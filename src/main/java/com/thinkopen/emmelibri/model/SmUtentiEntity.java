package com.thinkopen.emmelibri.model;

import javax.persistence.*;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_utenti", schema = "opty_sellout", catalog = "EXA_DB")
public class SmUtentiEntity {
    private String utente;
    private String password;

    @Id
    @Column(name = "utente", nullable = false, insertable = true, updatable = true, length = 100)
    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }

    @Basic
    @Column(name = "password", nullable = true, insertable = true, updatable = true, length = 100)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmUtentiEntity that = (SmUtentiEntity) o;

        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (utente != null ? !utente.equals(that.utente) : that.utente != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = utente != null ? utente.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
