package com.thinkopen.emmelibri.model;

import javax.persistence.*;
import java.math.BigInteger;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_l0_rese", schema = "opty_sellout", catalog = "EXA_DB")
@IdClass(SmL0ReseEntityPK.class)
public class SmL0ReseEntity {
    private BigInteger tpmov;
    private String catena;
    private String codicepdv;
    private String ean;
    private int qta;
    private String dataResa;
    private Integer annoResa;
    private BigInteger settimanaResa;

    @Basic
    @Column(name = "tpmov", nullable = true, insertable = true, updatable = true, precision = 0)
    public BigInteger getTpmov() {
        return tpmov;
    }

    public void setTpmov(BigInteger tpmov) {
        this.tpmov = tpmov;
    }

    @Basic
    @Column(name = "catena", nullable = true, insertable = true, updatable = true, length = 100)
    public String getCatena() {
        return catena;
    }

    public void setCatena(String catena) {
        this.catena = catena;
    }

    @Id
    @Column(name = "codicepdv", nullable = false, insertable = true, updatable = true, length = 10)
    public String getCodicepdv() {
        return codicepdv;
    }

    public void setCodicepdv(String codicepdv) {
        this.codicepdv = codicepdv;
    }

    @Id
    @Column(name = "ean", nullable = false, insertable = true, updatable = true, length = 13)
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Id
    @Column(name = "qta", nullable = false, insertable = true, updatable = true, precision = 0)
    public int getQta() {
        return qta;
    }

    public void setQta(int qta) {
        this.qta = qta;
    }

    @Id
    @Column(name = "data_resa", nullable = false, insertable = true, updatable = true, length = 8)
    public String getDataResa() {
        return dataResa;
    }

    public void setDataResa(String dataResa) {
        this.dataResa = dataResa;
    }

    @Basic
    @Column(name = "anno_resa", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getAnnoResa() {
        return annoResa;
    }

    public void setAnnoResa(Integer annoResa) {
        this.annoResa = annoResa;
    }

    @Basic
    @Column(name = "settimana_resa", nullable = true, insertable = true, updatable = true, precision = 0)
    public BigInteger getSettimanaResa() {
        return settimanaResa;
    }

    public void setSettimanaResa(BigInteger settimanaResa) {
        this.settimanaResa = settimanaResa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmL0ReseEntity that = (SmL0ReseEntity) o;

        if (qta != that.qta) return false;
        if (annoResa != null ? !annoResa.equals(that.annoResa) : that.annoResa != null) return false;
        if (catena != null ? !catena.equals(that.catena) : that.catena != null) return false;
        if (codicepdv != null ? !codicepdv.equals(that.codicepdv) : that.codicepdv != null) return false;
        if (dataResa != null ? !dataResa.equals(that.dataResa) : that.dataResa != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (settimanaResa != null ? !settimanaResa.equals(that.settimanaResa) : that.settimanaResa != null)
            return false;
        if (tpmov != null ? !tpmov.equals(that.tpmov) : that.tpmov != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tpmov != null ? tpmov.hashCode() : 0;
        result = 31 * result + (catena != null ? catena.hashCode() : 0);
        result = 31 * result + (codicepdv != null ? codicepdv.hashCode() : 0);
        result = 31 * result + (ean != null ? ean.hashCode() : 0);
        result = 31 * result + qta;
        result = 31 * result + (dataResa != null ? dataResa.hashCode() : 0);
        result = 31 * result + (annoResa != null ? annoResa.hashCode() : 0);
        result = 31 * result + (settimanaResa != null ? settimanaResa.hashCode() : 0);
        return result;
    }
}
