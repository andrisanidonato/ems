package com.thinkopen.emmelibri.repository;

import com.thinkopen.emmelibri.model.SmAcqOptyEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by donato on 9/19/17.
 */

public interface SmAqpOptyDao {

    List<SmAcqOptyEntity> findAll();



}
