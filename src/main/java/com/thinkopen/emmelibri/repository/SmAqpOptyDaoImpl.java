package com.thinkopen.emmelibri.repository;

import com.thinkopen.emmelibri.model.SmAcqOptyEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by donato on 9/19/17.
 */
@Repository
public class SmAqpOptyDaoImpl implements SmAqpOptyDao{


    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<SmAcqOptyEntity> findAll() {
        List<SmAcqOptyEntity> smAcqOptyEntities = new ArrayList<>();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(SmAcqOptyEntity.class);
            smAcqOptyEntities = (List<SmAcqOptyEntity>) criteria.list();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return smAcqOptyEntities;
    }
}
