package com.thinkopen.emmelibri.repository;

import com.thinkopen.emmelibri.model.SmAcqOptyEntity;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class Tester {
	
	public static void main(String[] args) {
		Configuration conf = new Configuration().configure();
		SessionFactory sessionFactory = conf.buildSessionFactory();
        List<SmAcqOptyEntity> smAcqOptyEntities = new ArrayList<>();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(SmAcqOptyEntity.class);
            smAcqOptyEntities = (List<SmAcqOptyEntity>) criteria.list();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

	}

}
